#!/bin/bash -e

#--------------
#- build game -
#--------------
mkdir -p build
mkdir -p bin
cd build
cmake -DCMAKE_TOOLCHAIN_FILE="toolchains/dos.cmake" .. 

# get number of cores + 1
NUM_WORKERS=$(grep -P '^processor\t: [0-9]+$' /proc/cpuinfo | wc -l)
make -j$NUM_WORKERS
cd ..
./upx -9 build/$1 || true 
#./upx --ultra-brute build/$1 || true 
cp build/$1 bin/$1.EXE

#---------------
#- build assets -
#----------------
cd src/tools/
g++ -o packer packer.cpp
cd ../..
touch bin/$1.DAT
src/tools/packer bin/$1.DAT || true

cp CWSDPMI.EXE bin/CWSDPMI.EXE

echo 'total game size:'
du -bh bin/
du -bh bin/$1.EXE
du -bh bin/$1.DAT
#stat --printf="%n -> %s\n" bin/*
