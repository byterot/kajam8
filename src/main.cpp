#include <iostream>
#include <sys/nearptr.h>
#include <cmath>
#include <vector>

#include <dos.h>

#include "renderer.h"
#include "vga.h"
#include "mesh.h"
#include "camera.h"
#include "inputdevice.h"
#include "face.h" 

int main() 
{
	if(!__djgpp_nearptr_enable()) 
  {
		printf("Could not enable nearptr. :(\n");
		return 1;
	}

  InputDevice input;


  
  VGA::init();
  VGA::fill(25);
  Renderer::init();

  Camera camera
  {
    glm::vec3(0,0,15),
    glm::vec3(0)
  };

  Mesh mesh
  {
    glm::vec3(0,0,0),
    glm::vec3(0,0,0),
    { // vertices
      glm::vec3(-1, 1, 1),
      glm::vec3(1, 1, 1),
      glm::vec3(-1, -1, 1),
      glm::vec3(1, -1, 1),
      glm::vec3(-1, 1, -1),
      glm::vec3(1, 1, -1),
      glm::vec3(1, -1, -1),
      glm::vec3(-1, -1, -1),
    },
    { // faces
      Face { 0, 1, 2 },
      Face { 1, 2, 3 },
      Face { 1, 3, 6 },
      Face { 1, 5, 6 },
      Face { 0, 1, 4 },
      Face { 1, 4, 5 },

      Face { 2, 3, 7 },
      Face { 3, 6, 7 },
      Face { 0, 2, 7 },
      Face { 0, 4, 7 },
      Face { 4, 5, 6 },
      Face { 4, 6, 7 },
    }
  };

  std::vector<Mesh> meshes;
  meshes.push_back(mesh);
  do
  {
    input.update();
    
    meshes.at(0).rotation.x += 0.01f;
    meshes.at(0).rotation.y += 0.01f;
    meshes.at(0).rotation.z += 0.01f;
    meshes.at(0).position.z += 0.01f;
    camera.position.z -= 0.01f;
    Renderer::render(camera, meshes);

    VGA::draw();
    Renderer::clear();
    VGA::fill(25);
  } while(!input.key_pressed(0x1));

  //delay(1000);

  Renderer::exit();
  VGA::exit();

	__djgpp_nearptr_disable();
	printf("Yes, it's buggy! back to work\n");
	return 0;
}
