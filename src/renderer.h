#pragma once

#include <vector>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdlib.h>

#include "vga.h"
#include "mesh.h"
#include "camera.h"

namespace Renderer
{
  extern float* zbuffer;

  void init()
  {
    zbuffer = (float*)malloc(VGA::VGA_STRIDE*VGA::VGA_HEIGHT * sizeof(float));
  }

  void exit()
  {
    free(zbuffer);
  }

  void clear();

  inline float clamp(float value, float min = 0.0f, float max = 1.0f)
  {
    if(value > max)
      return max;
    if(value < min)
      return min;

    return value;
  }
  inline float interpolate(float min, float max, float gradient)
  {
    return min + (max - min) * clamp(gradient);
  }

  glm::vec3 project(glm::vec3 const coord, glm::mat4 const matrix);

  void draw_point(glm::vec2 point, uint8_t color);

  void draw_line(glm::vec2 const& point0, glm::vec2 const& point1, uint8_t color);
  

  void draw_bline(glm::vec2 const& point0, glm::vec2 const& point1, uint8_t color);


  void scanline(int y, glm::vec3 const& pa, glm::vec3 const& pb, glm::vec3 const& pc, glm::vec3 const& pd, uint8_t color);

  void draw_triangle(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, uint8_t color);

  void render(Camera const& camera, std::vector<Mesh> const& meshes);
}

